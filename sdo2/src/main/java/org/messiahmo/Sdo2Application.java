package org.messiahmo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sdo2Application {

	public static void main(String[] args) {
		SpringApplication.run(Sdo2Application.class, args);
	}

}
