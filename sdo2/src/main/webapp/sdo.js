/*****************************
 * Global Declarations
 *****************************/

// Site up and Site down images
const SITE_UP_IMAGE = `<img src="up.svg" alt="site up">`;
const SITE_DOWN_IMAGE = `<img src="down.svg" alt="site down">`;

// SDO sites to check 
const sdoSitesArrayCorp = ['keystone', 'parts_mart', 'imc', 'lkq'];  
const sdoSitesArrayStore = ['keystone', 'parts_mart', 'imc', 'lkq'];
const sdoSitesArrayPr = ['keystone', 'parts_mart', 'imc', 'lkq'];

// Define internal sites
const PRODUCT_INFO_ID = "productinfo";
const PRODUCT_INFO_HEADING = "Product Information";
const PRODUCT_INFO_URL = "https://teamcluster.oreillyauto.com/wcm/Merchandise/ProductInfo/Main.html";

const ALL_SDO_ID = "allsdo";
const ALL_SDO_HEADING = "Supplier Direct Ordering";
const ALL_SDO_URL = "https://teamcluster.oreillyauto.com/SupplierDirectOrdering/SDO.do";

const SUPPLIER_INFO_ID = "suppliertechinfo";
const SUPPLIER_INFO_HEADING = "Supplier Search";
const SUPPLIER_INFO_URL = "https://teamcluster.oreillyauto.com/TechInfo/";
	
/**********************************************************************
 * Check All Sites
 * This method is called after the DOM is loaded to check sites and 
 * see if they are up or down. This method is recursive!
 **********************************************************************/
function checkAllSites() {
	// Initialize Site Count to 0
	siteCount = 0;
	
	// Pass the SDO Sites Array to the module method named checkSitesv2()
	sdoApiModule.checksites(sdoSitesArray);

	// Update up/down flag for EXTERNAL sites
	imageUptimeModule.checksites(externalSdoMap);
	
	// Update up/down flag for INTERNAL sites
	imageUptimeModule.checksites(internalSiteMap);

	// Some sites do not have images. We will check the heading instead.
	checkSiteByString(PRODUCT_INFO_ID, PRODUCT_INFO_HEADING, PRODUCT_INFO_URL);
	checkSiteByString(ALL_SDO_ID, ALL_SDO_HEADING, ALL_SDO_URL);
	checkSiteByString(SUPPLIER_INFO_ID, SUPPLIER_INFO_HEADING, SUPPLIER_INFO_URL);
	
	// Update the last updated date on the page
	updateTimestamp();
}

/*******************************************************************************
 * SDO API MODULE 
 * This module is used to assess if SDO sites are up or down.
 * 
 * Access a supplier directly:
 *    https://store-monitor.oreillyauto.com/sdo?keystone
 *
 * Use underscores where applicable:
 *    https://store-monitor.oreillyauto.com/sdo?land_n_sea
 *
 * Complete list of SDO site references:
 *    https://store-monitor.oreillyauto.com/sdo?sites
 *		
 * Status of ALL SDO sites
 *    https://store-monitor.oreillyauto.com/sdo
 *
 * Current SDO sites via API (11/29/2021)
 *    ["axalta","covercraft","coverking","imc","isn","keystone",
 * 	   "land_n_sea","lkq","medco","national_oak","parts_mart",
 * 	   "sma","ssf"]
 * 
 * Current SDO sites via API on this page (11/29/2021):   
 *    ["keystone", "parts_mart", "imc", "lkq", "dorman", "fleetcross"]   
 *    
 ********************************************************************************/
var sdoApiModule = {	
		 
	/**
	 * Check Sites V2
	 * This method takes one parameter:
	 *	 Parameter 1: an array of element IDs that need to be updated
	 * This method calls the SDO API to obtain an object with an array of objects 
	 * and then processes the response to determine if the sites are up or down.
	 */			 
	checksites : (sdoSitesArray)=> {
		// Validate the array that was passed into this function
		if (sdoSitesArray == undefined || !Array.isArray(sdoSitesArray) || sdoSitesArray.length == 0) {
			return;
		}
		
		// Let's use the all encompasing service call to get all of the statuses at one time
		let url = 'https://store-monitor.oreillyauto.com/sdo';
		
		fetch(url)
		.then((response)=> {
			if (response.ok) { // check for http status code 200 
				return response.json();
			} else {
				// API Status error (e.g. Http 500 internal error).
				console.error("API Check failed. " + "Response code=" + response.status);
			}
		}).then((result)=> {
			if (result) {
				// Get the array from the response
				let resultArray = result.sdo_sites;
				
				// Call the method to process the response
				sdoApiModule.processArrayOfSdoSites(resultArray, sdoSitesArray);
			} else {
				// API Result Exception. Nothing came back!
				console.error("API Response Failed. result=" + result);
			}
		}).catch((error)=> {
			console.error('Error! error fetching '+url+'. error='+error);
			// API Communication Exception (can't reach server)
		});
	},

	/**
	 * Process Array of SDO Sites
	 * This method takes two parameters:
     *   Parameter 1: an array of results from the SDO API 
	 *   Parameter 2: an array of element IDs that need to be updated
	 */
	processArrayOfSdoSites : (resultArray, sdoSitesArray) => {
		for (let siteObject of resultArray) {
			let siteName = siteObject.site;
			let siteStatus = siteObject.status;
			let elementId = sdoApiModule.getElementIdBySdoSiteName(siteName);
			let element = document.getElementById(elementId);
			
			if (element) {
				if ("down" == siteStatus) {
					updateImageOnElement(element, SITE_DOWN_IMAGE);
				} else {
					updateImageOnElement(element, SITE_UP_IMAGE);
				}
			}
		}
	},
	
	/**
	 * Get Element ID By SDO Site Name
	 * Some site names have spaces or special characters. This method will
	 * take a site name and convert it to the ID on the target element you 
	 * wish to modify.
	 */
	getElementIdBySdoSiteName : (siteName)=> {			
		switch(siteName) {
			case "Axalta - Store Ordering Access" : return "axalta_store";
			case "Beck Arnley - Store Ordering Access" : return "beck_arnley_store";
			case "Covercraft - Store Ordering Access" : return "covercraft_store";
			case "Covercraft - O'Reilly Corporate Access" : return "covercraft_corp";
			case "Coverking - Store Ordering Access" : return "coverking_store";
			case "Coverking - O'Reilly Corporate Access" : return "coverking_corp";
			case "Grainger - O'Reilly Corporate Access" : return "grainger_corp";
			case "Grainger - Store Resale Account Ordering Access" : return "grainger_store_resale";
			case "Grainger - Store Supplies Ordering Access" : return "grainger_store_supplies";
			case "IMC - O'Reilly Corporate Access" : return "imc_corp";
			case "IMC - Store Ordering Access" : return "imc_store";
			case "ISN - O'Reilly Corporate Access" : return "isn_corp";
			case "ISN - Store Ordering Access" : return "isn_store";
			case "Keystone - O'Reilly Corporate Access" : return "keystone_corp";
			case "Keystone - O'Reilly Sales Dept. Access" : return "keystone_sales";
			case "Keystone - Store Ordering Access" : return "keystone_store";
			case "Land 'N Sea - O'Reilly Corporate Access" : return "land_n_sea_corp";
			case "Land 'N Sea - Store Ordering Access" : return "land_n_sea_store";
			case "LKQ - O'Reilly Corporate Access" : return "lkq_corp";
			case "LKQ - Store Ordering Access" : return "lkq_store";
			case "Medco - O'Reilly Corporate Access" : return "medco_corp";
			case "Medco - Store Ordering Access" : return "medco_store";
			case "National Oak - O'Reilly Corporate Access" : return "national_oak_corp";
			case "National Oak  - Store Ordering Access" : return "national_oak_store";
			case "National Parts Depot - Store Ordering Access" : return "nation_parts_depot_store";
			case "Parts Mart - O'Reilly Corporate Access" : return "parts_mart_corp";
			case "Parts Mart - Store Ordering Access" : return "parts_mart_store";				
			case "SMA - O'Reilly Corporate Access" : return "sma_corp";
			case "SMA - Store Ordering Access" : return "sma_store";
			case "SSF - O'Reilly Corporate Access" : return "ssf_corp";
			case "SSF - Store Ordering Access" : return "ssf_store";
			default: return "";
		}
	}
};

/**
 * IMAGE UPTIME MODULE
 * This module takes a map and determines if a site is up or down based on the 
 * image provided. The image should be the site's favicon.ico if one exists. If
 * not, it should be an image on the landing page or login page.
 * 
 * The map that is passed in should contain the following key/value pairs:
 *   Key   : The element ID that needs an up or down image (via innerHTML)
 *   Value : The favicon.ico or image on the landing page or login page. 
 */
var imageUptimeModule = {
    
	checksites : (map)=>{
		try {
			if (map == undefined || !map instanceof Map || map.length == 0) {
				return;
			}
		    
			map.forEach((value, key, map)=> {
				// Define local variables
				let id=key;
				let targetImage = value;
				let container = document.getElementById(id);
				
				// If we do not have a valid container, skip iteration
				if (!container) {
					return false; // normally you would see "continue;" but return false is correct
				}
				
				// Create the img element, add the source, and check if loaded
			    var img=document.createElement('img');
				img.src=targetImage;	
			    
			    // If image loads, site is up!
			    img.onload=function() {
					//container.innerHTML = imageUptimeModule.siteUpImage;
					updateImageOnElement(container, SITE_UP_IMAGE);
			    };
			    
			    // If image does not load, site is down; add red border
			    img.onerror=function() {
			    	//container.innerHTML = imageUptimeModule.siteDownImage;
			    	updateImageOnElement(container, SITE_DOWN_IMAGE);
			    }; 
			});
		} catch (error) {
			console.error("try-catch-error (imageUptimeModule.checkSites()).  Error=" + error);
		}	
	}	
};

/**
 * Update Image On Element
 * This method checks to see if the new image element matches
 * the previous image element. If so, the image is left alone.
 * If not, the old element is removed and a new element is left
 * in its place.
 * 
 * This method takes two parameters:
 *   Parameter 1: The element that contains the current image
 *   Parameter 2: The new image (in HTML format)
 */
function updateImageOnElement(element, imageHtml) {
	siteCount+=1;
	console.log("siteCount="+siteCount);	 

	if (imageHtml && element) {
		let elementInnerHTML = element.innerHTML;
		
		if (elementInnerHTML.trim() != imageHtml.trim()) {
			let imgNode = element.children[0];  // <img src=...
			
			if (imgNode) {
				element.removeChild(imgNode);	
			}
			
			element.innerHTML = imageHtml;
		}
	}
}
 
/**
 * UPDATE TIMESTAMP
 * This method simply updates the timestamp in the bottom right of the 
 * page with the current date and time.
 */
function updateTimestamp() {
	lastUpdateDate.innerHTML = "";
	lastUpdateDate.innerHTML = getCurrentDateAndTime();	
}

/**
 *  GET EXTERNAL SDO MAP
 *  This function obtains a map of keys and values.
 *  The key represents the external site and the 
 *  value represents an image on the site to check
 *  to test if the site is up or not.
 */
function getExternalSdoMap() {
	let sdoMap = new Map();
	sdoMap.set("dorman", "https://www.dormanproducts.com/favicon.ico");
	sdoMap.set("gates", "https://cms.gates.com/~/media/images/microsites/oreilly/gates_logo_tagline_lockup_digital_2color_black_350x65.png");
	sdoMap.set("smp", "https://eaccess.smpcorp.com/eCatalogs/STD/Images/STD_logo.jpg");
	sdoMap.set("wix", "https://lookupfilters.com/App_Themes/Default/images/Oreilly%20header2.jpg");		
	return sdoMap;
}

/**
 *  This function obtains a map of keys and values.
 *  The key represents the element ID of the element to 
 *  hold the up or down image and the value represents 
 *  an image on the site to check to test if the site is 
 *  up or not. e.g. <div id="oreilly"><img src....
 */
function getInternalSiteMap() {
	let map = new Map();
	map.set("fco", "https://www.firstcallonline.com/FirstCallOnline/images/fco-logo-notag.png");
	map.set("fleetcross", "https://v4.fleetcross.net/Images/logo_MOTOR.png");
	map.set("loanertools", "https://teamcluster.oreillyauto.com/wcm/Training/LoanerToolProgram/images/loanertool_banner.png");
	map.set("oreilly", "https://www.oreillyauto.com/favicon.ico");
	map.set("vehiclehelp", "https://teamcluster.oreillyauto.com/wcm/Images/Logos/VehicleHelp.jpg");
	map.set("digitalcatalograck", "https://www.oreillyautocatalogs.com/skin/images/logo_big.png");
	return map;
}


/******************************************************
 * Check Site By String
 * In some cases, we cannot check to see if a site is up using the API
 * or by checking an image. In this case, we need to get the contents of
 * the page and see if the intended string is on the page. If so, we can 
 * guess that the page/site is up.
 * 
 * This method takes three parameters:
 *   Parameter 1: The ID for the element that needs the up or down image
 *   Parameter 2: The string we are searching for on the page we fetch
 *   Parameter 3: The web address of the page we need to inspect
 ******************************************************/
function checkSiteByString(elementId, string, url) {		
	if (elementId && string && url) {
		fetch(url)
		.then((response)=> {
			if (response.ok) { 
				return response.text();
			}
		}).then((html)=> {
			let regex = new RegExp(string,'gim'); // Global, case Insensitive, and Multiline
			let result = html.match(regex);
			let element = document.getElementById(elementId);
			
			if (element) {
				if (result == null || result == undefined || result.length == 0) {
					//element.innerHTML = SITE_DOWN_IMAGE;
					updateImageOnElement(element, SITE_DOWN_IMAGE);
				} else {
					//element.innerHTML = SITE_UP_IMAGE;
					updateImageOnElement(element, SITE_UP_IMAGE);
				}	
			}
		}).catch((error)=> {
			console.error('Error! error fetching '+url+'. error='+error);
			// API Communication Exception (can't reach server)
		});
	}
}

/********************************************************
 * Get Current Date and Time
 * This method is used to set the last update date in the
 * bottom right of the page.
 ********************************************************/
function getCurrentDateAndTime() {
	var d = new Date();
	var minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes();
	var hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours();
	//var ampm = d.getHours() >= 12 ? 'pm' : 'am';
	var date = d.getDate().toString().length == 1 ? '0'+d.getDate() : d.getDate();
	var month = d.getMonth() + 1;
	var year = d.getFullYear();
	month = month.toString().length == 1 ? '0'+month : month;
    return month+"/"+date+"/"+year+" "+hours+':'+minutes/*+ampm*/;
}